import { IsNotEmpty, MinLength, IsNegative, IsPositive } from 'class-validator';
export class CreateProductDto {
  id: number;

  @MinLength(8)
  name: string;

  @IsPositive()
  price: number;
}
